//
//  RWDeck.m
//  CCG
//
//  Created by Brian Broom on 2/26/14.
//  Copyright (c) 2014 Brian Broom. All rights reserved.
//

#import "RWDeck.h"
#import "RWCard.h"
#import "RWCardNames.h"
#import "RWGameScene.h"

@interface RWDeck ()

@property (strong, nonatomic) NSMutableArray *cards;
@property (strong, nonatomic) SKTexture *cardBack;
@property (strong, nonatomic) SKTexture *highlightCardBack;
@property (weak, nonatomic, readonly) RWGameScene *gameScene;

@end

@implementation RWDeck

- (id)initWithDataArray:(NSArray *)cardNumberArray {
    self = [super initWithImageNamed:@"card_back.png"];
    if (self) {
        _cardBack = self.texture;
        _highlightCardBack = [SKTexture textureWithImageNamed:@"card_back_sel.png"];
        
        _cards = [NSMutableArray new];
        for (id data in cardNumberArray) {
            NSInteger cardNumber = [data intValue];
            [_cards addObject:[RWCard cardWithCardName:cardNumber]];
        }
        
        //[self shuffle];
        while (YES) {
            [self shuffle];
            if ([self isGoodShuffle]) { break; }
        }
        
        self.userInteractionEnabled = YES;
        //self.active = YES;
        //[self energyProbability];
    }
    return self;
}

- (RWGameScene *)gameScene {
    return (RWGameScene *)self.scene;
}

- (void)energyProbability {
    NSInteger goodCount=0;
    for (NSInteger i=0; i<1000; i++) {
        RWCard *firstCard = self.cards[0];
        RWCard *secondCard = self.cards[1];
        RWCard *thirdCard = self.cards[2];
        
        if ([firstCard.cardType isEqualToString:@"Energy"]  ||
            [secondCard.cardType isEqualToString:@"Energy"] ||
            [thirdCard.cardType isEqualToString:@"Energy"]) {
            goodCount++;
        }

        
    }
    NSLog(@"%ld good out of 1000 runs: %f percent", (long)goodCount, goodCount*100.0/1000.0);
}

- (BOOL)isGoodShuffle {
    RWCard *firstCard = self.cards[0];
    RWCard *secondCard = self.cards[1];
    RWCard *thirdCard = self.cards[2];
    
    return ([firstCard.cardType isEqualToString:@"Energy"]  ||
            [secondCard.cardType isEqualToString:@"Energy"] ||
            [thirdCard.cardType isEqualToString:@"Energy"]);

}

#pragma  mark - Deck Mechanics
- (void)shuffle {
    //NSLog(@"Shuffle");
    // from http://nshipster.com/random/
    NSUInteger count = [self.cards count];
    // See http://en.wikipedia.org/wiki/Fisher–Yates_shuffle
    if (count > 1) {
        for (NSUInteger i = count - 1; i > 0; --i) {
            [self.cards exchangeObjectAtIndex:i withObjectAtIndex:arc4random_uniform((int32_t)(i + 1))];
        }
    }
}

- (RWCard *)drawCard {
    
    RWCard *drawnCard;
    
    if ([self.cards count] > 0) {
        drawnCard = [self.cards firstObject];
        [self.cards removeObjectAtIndex:0];
        drawnCard.zRotation = self.zRotation;
        drawnCard.position = self.position;
        drawnCard.player = self.player;
        
        [self runAction:[SKAction playSoundFileNamed:@"deal.wav" waitForCompletion:NO]];
        
        [(RWGameScene *)self.scene addChild:drawnCard];
    }
    
    if ([self.cards count] == 0) {
        self.texture = [SKTexture textureWithImageNamed:@"clear.png"];
    }
    
    return drawnCard;
}

@end
