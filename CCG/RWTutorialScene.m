//
//  RWTutorialScene.m
//  CCG
//
//  Created by Brian Broom on 2/25/14.
//  Copyright (c) 2014 Brian Broom. All rights reserved.
//

@import AVFoundation;

#import "RWTutorialScene.h"
#import "RWMenuScene.h"

@interface RWTutorialScene ()

@property (strong, nonatomic) AVAudioPlayer *musicPlayer;
@property (strong, nonatomic) NSArray *images;
@property (assign, nonatomic) NSInteger index;
@property (assign, nonatomic, getter = isSliding) BOOL sliding;

@end

@implementation RWTutorialScene

-(id)initWithSize:(CGSize)size {
    if (self = [super initWithSize:size]) {
        // sound setup
        // play background music
        NSString *path = [[NSBundle mainBundle] pathForResource:@"Pippin the Hunchback" ofType:@"mp3"];
        NSURL *musicFile = [[NSURL alloc] initFileURLWithPath:path];
        NSError *error = nil;
        _musicPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:musicFile  error:&error];
        _musicPlayer.numberOfLoops = -1; // negative value repeats indefinitely
        [_musicPlayer prepareToPlay];
        [_musicPlayer play];
        
        _images = @[@"draw.png", @"playEnergy.png",@"changePhase.png", @"activateEnergy.png", @"castCreatures.png", @"attack.png"];
        
        // button to return to menu
        SKSpriteNode *menuButton = [SKSpriteNode spriteNodeWithImageNamed:@"MenuButton.png"];
        menuButton.name = @"menu";
        menuButton.zPosition = 10;
        menuButton.position = CGPointMake(CGRectGetMidX(self.frame), 40);
        [self addChild:menuButton];
        
        // first tutorial screen
        SKSpriteNode *screenOne = [SKSpriteNode spriteNodeWithImageNamed:_images[0]];
        screenOne.name = @"onScreen";
        screenOne.position = CGPointMake(512, 384);
        
        // second tutorial screen
        
        SKSpriteNode *screenTwo = [SKSpriteNode spriteNodeWithImageNamed:_images[1]];
        screenTwo.name = @"offScreen";
        screenTwo.position = CGPointMake(1536, 384);
 
        _index = 0;
        
        [self addChild:screenOne];
        [self addChild:screenTwo];
    }
    return self;
}

- (void)nextImage {
    self.index += 1;
    if (self.index + 1 == [self.images count]) {
        self.index = 0;
    }
    
//    SKSpriteNode *onScreen = (SKSpriteNode *)[self childNodeWithName:@"onScreen"];
//    [onScreen removeFromParent];
    
    SKSpriteNode *offScreen = (SKSpriteNode *)[self childNodeWithName:@"offScreen"];
    offScreen.name = @"onScreen";
    
    SKSpriteNode *newScreen = [SKSpriteNode spriteNodeWithImageNamed:self.images[self.index+1]];
    newScreen.name = @"offScreen";
    newScreen.position = CGPointMake(1536, 384);
    [self addChild:newScreen];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    // exit early if still animating
    if (self.isSliding) { return; }
    
    // slide text off screen
    self.sliding = YES;
    SKAction *slideLeft = [SKAction moveByX:-1024 y:0 duration:0.3]; // move a full screen width left
    SKAction *remove = [SKAction removeFromParent];
    [[self childNodeWithName:@"onScreen"] runAction:[SKAction sequence:@[slideLeft, remove]] completion:^{
        _sliding = NO;
    }];
    [[self childNodeWithName:@"offScreen"] runAction:slideLeft];
    
    UITouch *touch = [touches anyObject];
    CGPoint location = [touch locationInNode:self];
    SKNode *touchNode = [self nodeAtPoint:location];
    
    SKAction *pushDown = [SKAction scaleTo:0.8 duration:0.2];
    SKAction *click = [SKAction playSoundFileNamed:@"click.wav" waitForCompletion:YES];
    SKAction *pushUp = [SKAction scaleTo:1.0 duration:0.1];
    
    SKAction *clickAndUp = [SKAction group:@[click, pushUp]];
    SKAction *push = [SKAction sequence:@[pushDown, clickAndUp]];
    
    if ([touchNode.name isEqualToString:@"menu"]) {
        [touchNode runAction:push completion:^{
            [self.musicPlayer stop];
            SKScene *tutorialScene = [[RWMenuScene alloc] initWithSize:self.size];
            [self.view presentScene:tutorialScene];
        }];
        return;
    }
    
    [self nextImage];
}
@end
