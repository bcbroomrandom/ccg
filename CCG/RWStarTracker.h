//
//  RWStarTracker.h
//  CCG
//
//  Created by Brian Broom on 2/27/14.
//  Copyright (c) 2014 Brian Broom. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

@interface RWStarTracker : SKSpriteNode

@property (assign, nonatomic) NSInteger stars;

- (BOOL)canAddStar;
- (void)addStar;
- (void)removeStars:(NSInteger)used;
- (void)resetStars;

@end
