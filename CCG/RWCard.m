//
//  RWCard.m
//  CCG
//
//  Created by Brian Broom on 2/26/14.
//  Copyright (c) 2014 Brian Broom. All rights reserved.
//

#import "RWCard.h"
#import "RWCardNames.h"
#import "RWGameScene.h"
#import "RWPlayer.h"
#import "RWEnergyTracker.h"
#import "RWCardRegion.h"
#import "RWPlayerTracker.h"
#import "RWInPlayRegion.h"


@interface RWCard () {
    NSInteger _attackValue;
    NSInteger _defenseValue;
}

@property (strong, nonatomic) SKTexture *frontTexture;
@property (strong, nonatomic) SKTexture *highlightTexture;
@property (strong, nonatomic) SKTexture *backTexture;
@property (assign, nonatomic) CGPoint savedPosition;
@property (assign, nonatomic) NSInteger savedZPosition;
@property (assign, nonatomic) CGFloat savedZRotation;
@property (weak, nonatomic, readonly) RWGameScene *gameScene;
@property (strong, nonatomic) SKLabelNode *damageLabel;
@property (strong, nonatomic) NSMutableArray *attachedCards;
@property (assign, nonatomic) NSInteger discardOnTurn;
@property (assign, nonatomic) NSInteger spellDuration;

@end

@implementation RWCard

+ (instancetype)cardWithCardName:(RWCardName)name {
    return [[RWCard alloc] initWithCardName:name];
}

- (instancetype)initWithCardName:(RWCardName)name {
    self = [super initWithImageNamed:@"card_back.png"];
    if (self) {
        _backTexture = self.texture;
        _active = YES;
        _cardName = name;
        _attachedCards = [NSMutableArray new];
        
        _damageLabel = [SKLabelNode labelNodeWithFontNamed:@"OpenSans-Bold"];
        _damageLabel.fontSize = 12;
        _damageLabel.fontColor = [UIColor colorWithRed:0.47 green:0.0 blue:0.0 alpha:1.0];
        _damageLabel.text = @"0";
        _damageLabel.hidden = YES;
        _damageLabel.position = CGPointMake(25, 40);
        [self addChild:_damageLabel];
        
        switch (name) {
            case CreatureBear:
                _frontTexture = [SKTexture textureWithImageNamed:@"card_creature_bear.png"];
                _cardType = @"Creature";
                _largeCardFileName = @"card_creature_bear_large.png";
                _highlightTexture = [SKTexture textureWithImageNamed:@"card_creature_bear_sel.png"];
                _energyCost = 2;
                _attackValue = 2;
                _defenseValue = 2;
                break;
                
            case CreatureDragon:
                _frontTexture = [SKTexture textureWithImageNamed:@"card_creature_dragon.png"];
                _cardType = @"Creature";
                _largeCardFileName = @"card_creature_dragon_large.png";
                _highlightTexture = [SKTexture textureWithImageNamed:@"card_creature_dragon_sel.png"];
                _energyCost = 3;
                _attackValue = 3;
                _defenseValue = 3;
                break;
                
            case CreatureWolf:
                _frontTexture = [SKTexture textureWithImageNamed:@"card_creature_wolf.png"];
                _cardType = @"Creature";
                _largeCardFileName = @"card_creature_wolf_large.png";
                _highlightTexture = [SKTexture textureWithImageNamed:@"card_creature_wolf_sel.png"];
                _energyCost = 1;
                _attackValue = 1;
                _defenseValue = 1;
                break;
                
            case Energy:
                _frontTexture = [SKTexture textureWithImageNamed:@"card_energy.png"];
                _cardType = @"Energy";
                _largeCardFileName = @"card_energy_large.png";
                _highlightTexture = [SKTexture textureWithImageNamed:@"card_energy.png"];
                break;
                
            case SpellDeathRay:
                _frontTexture = [SKTexture textureWithImageNamed:@"card_spell_death_ray.png"];
                _cardType = @"Spell";
                _spellTargets = @"Enemy";
                _largeCardFileName = @"card_spell_death_ray_large.png";
                _highlightTexture = [SKTexture textureWithImageNamed:@"card_spell_death_ray_sel.png"];
                _energyCost = 2;
                break;
                
            case SpellRabid:
                _frontTexture = [SKTexture textureWithImageNamed:@"card_spell_rabid.png"];
                _cardType = @"Spell";
                _spellTargets = @"Friendly";
                _largeCardFileName = @"card_spell_rabid_large.png";
                _highlightTexture = [SKTexture textureWithImageNamed:@"card_spell_rabid_sel.png"];
                _energyCost = 1;
                _attackValue = 2;
                _defenseValue = 2;
                _spellDuration = 1;
                break;
                
            case SpellSleep:
                _frontTexture = [SKTexture textureWithImageNamed:@"card_spell_sleep.png"];
                _cardType = @"Spell";
                _spellTargets = @"Enemy";
                _largeCardFileName = @"card_spell_sleep_large.png";
                _highlightTexture = [SKTexture textureWithImageNamed:@"card_spell_sleep_sel.png"];
                _energyCost = 1;
                _spellDuration = 1;
                break;
                
            case SpellStoneskin:
                _frontTexture = [SKTexture textureWithImageNamed:@"card_spell_stoneskin.png"];
                _cardType = @"Spell";
                _spellTargets = @"Friendly";
                _largeCardFileName = @"card_spell_stoneskin_large.png";
                _highlightTexture = [SKTexture textureWithImageNamed:@"card_spell_stoneskin_sel.png"];
                _energyCost = 1;
                _attackValue = 0;
                _defenseValue = 2;
                // should use a flag like -1, setting to really big value for "never"
                _spellDuration = 1000000;
                break;
                
            default:
                NSLog(@"Invalid Card Name.");
                break;
        }
        self.texture = _frontTexture;
        self.userInteractionEnabled = YES;
    }
    return self;
}

#pragma mark - Custom Getters and Setters

- (RWGameScene *)gameScene {
    return (RWGameScene *)self.scene;
}

- (CGFloat)rotationSign {
    
    // this method is to help with positioning of P1 and P2 elements
    // since P2 elements area drawn upside down, moving a card "up" as
    // in attachments means they should move "down" in screen coords.
    // in RWCard would be used something like
    // position.y = location + self.player.rotationSign * 18.0
    // which will move a card up for P1, and down for P2
    // without putting all of those methods in if blocks
    
    if (self.zRotation > 1.5) {
        return -1;
    } else {
        return 1;
    }
}

- (void)doDamage:(NSInteger)damage {
    _damage += damage;
    self.damageLabel.text = [NSString stringWithFormat:@"-%ld", (long)_damage];
    if (_damage != 0) { self.damageLabel.hidden = NO; }
}

- (void)setZPosition:(CGFloat)zPosition {
    [super setZPosition:zPosition];
    for (RWCard *card in self.attachedCards) {
        NSInteger index = [self.attachedCards indexOfObject:card];
        card.zPosition = zPosition - (index + 1);
    }
}

- (NSInteger)attackValue {
    NSInteger total = _attackValue;
    for (RWCard *card in self.attachedCards) {
        total += card.attackValue;
    }
    return total;
}

- (NSInteger)defenseValue {
    NSInteger total = _defenseValue;
    for (RWCard *card in self.attachedCards) {
        total += card.defenseValue;
    }
    return total;
}

#pragma mark - Card Actions
- (void)faceUp {
    self.texture = self.frontTexture;
}

- (void)faceDown {
    self.texture = self.backTexture;
}

- (void)highlight {
    self.texture = self.highlightTexture;
}

- (void)clearHilight {
    self.texture = self.frontTexture;
}

- (void)cardHint {
    if ([self.cardType isEqualToString:@"Energy"]) {
        [self.gameScene notifyWithString:@"Drag Energy card to the energy region"];
    }
    
    if ([self.cardType isEqualToString:@"Creature"]){
        [self.gameScene notifyWithString:@"Drag Creature cards to the center of the board to summon"];
    }
    
    if ([self.cardType isEqualToString:@"Spell"]) {
        [self.gameScene notifyWithString:@"Drag Spell cards to the center of the board to cast"];
    }
}

- (void)discard {
    [(SKNode<RWCardRegion> *)self.player.discard addCard:self fromRegion:self.currentRegion];
}

#pragma mark - Attached Cards

- (void)attachCard:(RWCard *)attachment {
    [attachment.currentRegion removeCard:attachment];
    
    if (attachment.cardName == SpellSleep) {
        self.incapacitated = YES;
    }
    attachment.discardOnTurn = self.gameScene.turnNumber + attachment.spellDuration;
    
    [self.attachedCards addObject:attachment];
    
    NSInteger index = [self.attachedCards indexOfObject:attachment];
    attachment.position = CGPointMake(self.position.x, self.position.y + self.rotationSign * 18 * (index + 1));
    attachment.zPosition = self.zPosition - (index + 1);
    attachment.zRotation = self.zRotation;
    attachment.savedZRotation = self.zRotation;
    [attachment removeActionForKey:@"rotate"];    
}

- (void)haveAttachmentsExpired {
    NSMutableArray *cardsToDiscard = [NSMutableArray new];
    
    for (RWCard *attachment in self.attachedCards) {
        if (self.gameScene.turnNumber >= attachment.discardOnTurn) {
            [cardsToDiscard addObject:attachment];
        }
    }
    
    for (RWCard *attachment in cardsToDiscard) {
        [self discardAttachedCard:attachment];
    }
    [self restackAttachments];
}

- (void)discardAttachedCard:(RWCard *)attachment {
    if (attachment.cardName == SpellSleep) {
        self.incapacitated = NO;
    }
    [self.attachedCards removeObject:attachment];
    [attachment discard];
    if (self.damage >= self.defenseValue) {
        [self discard];
    }
}

- (void)discardAllAttachemnts {
    for (RWCard *card in self.attachedCards) {
        [card discard];
    }
    [self.attachedCards removeAllObjects];
}

- (void)restackAttachments {
    for (RWCard *attachment in self.attachedCards) {
        NSInteger index = [self.attachedCards indexOfObject:attachment];
        CGPoint newPosition = CGPointMake(self.position.x, self.position.y + self.rotationSign * 18.0 * (index + 1));
        attachment.position = newPosition;
    }
}

- (void)returnToHomePosition {
    SKAction *returnToPreMovePosition = [SKAction moveTo:self.preMovePosition duration:0.3];
    [self runAction:returnToPreMovePosition completion:^{
        self.zPosition = self.savedZPosition;
        [self restackAttachments];
    }];
}

#pragma mark - Touch Handlers

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [touches anyObject];
    
    self.savedZRotation = self.zRotation;
    [self runAction:[SKAction scaleTo:1.5 duration:0.2]];
    self.moved = NO;
    
    // hack so dragging card goes on top of other cards
    // set to 19 so its not above the tracker layers (20)
    self.savedZPosition = self.zPosition;
    self.zPosition = 19;
    self.preMovePosition = self.position;
    
    // inactive cards include used energy cards, can't use again
    if (!self.active) { return; }
    
    if ([self.gameScene.subPhase isEqualToString:@"SelectEnemyTarget"] &&
        self.player != self.gameScene.currentPlayer) {
        [self.gameScene castSpellOnTarget:self];
        return;
    }
    
    if ([self.gameScene.subPhase isEqualToString:@"SelectFriendlyTarget"] &&
        self.player == self.gameScene.currentPlayer) {
        [self.gameScene castSpellOnTarget:self];
        return;
    }
    
    // double tap shows large version of the card
    // check to make sure the tapped card belongs to current player - No Peeking!
    // added check for not in EnergyTracker region. Since card movement is automatic in that region
    // tap on card will move it. Double tap in same place will trigger enlarged card on second energy card.
    if ( (touch.tapCount >= 2) &&
        (self.player == self.gameScene.currentPlayer || ![self.currentRegion.name isEqualToString:@"Hand"] ) &&
        (self.currentRegion != self.player.energyTracker) ) {
        [self.gameScene showLargeCard:self];
    }
    
    // shouldn't activate cards from defending player, exit early
    // if need touches on defenderPlayer cards, put before here
    if (self.gameScene.currentPlayer != self.player) { return; }
    
    
    

    
    if ([self.cardType isEqualToString:@"Energy"] && [self.currentRegion.name isEqualToString:@"Energy Tracker"]) {
        [self.player.energyTracker addStarFromCard:self];
    }
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    
    if (![self actionForKey:@"wiggle"]) {
        SKAction *rotR = [SKAction rotateByAngle:0.15 duration:0.2];
        SKAction *rotL = [SKAction rotateByAngle:-0.15 duration:0.2];
        SKAction *cycle = [SKAction sequence:@[rotR, rotL, rotL, rotR]];
        SKAction *wiggle = [SKAction repeatActionForever:cycle];
        [self runAction:wiggle withKey:@"wiggle"];
    }
    
    // shouldn't move cards from defending player, exit early
    if (self.gameScene.currentPlayer != self.player) { return; }
    // in-play energy cards shouldn't move
    if (self.currentRegion == self.player.energyTracker) { return; }
    
    UITouch *touch = [touches anyObject];
    CGPoint location = [touch locationInNode:self.scene];
    
    self.moved = YES;
    self.position = location;
    
    // also change position for attached cards
    [self restackAttachments];
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    [self runAction:[SKAction scaleTo:1.0 duration:0.2]];
    [self removeActionForKey:@"wiggle"];
    [self runAction:[SKAction rotateToAngle:self.savedZRotation duration:0.2] withKey:@"rotate"];
    //NSLog(@"x: %f, y: %f", self.position.x, self.position.y);
    
    // return card to previous zPosition
    // activated energy cards have been reassigned a zposition
    // for stacking issues. Moving card zPosition should be 19
    // If zPos is not 19, don't change it
    if (self.zPosition == 19) {
        self.zPosition = self.savedZPosition;
    }
    
    // energy cards movement is taken care of in EnergyTracker
    // exit early
    if ([self.cardType isEqualToString:@"Energy"] && [self.currentRegion.name isEqualToString:@"Energy Tracker"]) {
        return;
    }
    
    if (!self.moved) { return; }
    
    UITouch *touch = [touches anyObject];
    CGPoint location = [touch locationInNode:self.scene];
    SKNode<RWCardRegion> *dropRegion = [self.gameScene regionForPoint:location];
    
    // creature card dragged over player tracker -> attack other player
    if ([self.cardType isEqualToString:@"Creature"] &&
        [self.gameScene.playerTracker containsPoint:self.position]) {
        
        if (self.isIncapacitated) {
            [self.gameScene notifyWithString:@"Creature cannot attack"];
        }
        else {
            [self.gameScene fightWithAttacker:self andDefender:nil];
        }
        
        [self returnToHomePosition];
        return;
    }
    
    // Card is a creature, is already in play, dropped in an inplay region
    // most likely you want to attack - need to check
    if ([self.cardType isEqualToString:@"Creature"] &&
        [dropRegion.name isEqualToString:@"InPlay"] &&
        [self.currentRegion.name isEqualToString:@"InPlay"]) {
        
        if (self.isIncapacitated) {
            [self.gameScene notifyWithString:@"Creature cannot attack"];
            [self returnToHomePosition];
            return;
        }
        
        // might be nil
        RWCard *cardDroppedOn = [(RWInPlayRegion *)dropRegion cardAtPoint:self.position];
        
        if (!cardDroppedOn) {
            [self.gameScene notifyWithString:@"Drag onto creature or player to attack"];
        }
        else {
            if ([cardDroppedOn.cardType isEqualToString:@"Creature"] &&
                (self.player != cardDroppedOn.player)) {
                    [self.gameScene fightWithAttacker:self andDefender:cardDroppedOn];
                }
        }
        
        [self returnToHomePosition];
        return;
    }
    
    if ([self.cardType isEqualToString:@"Spell"] &&
        [dropRegion.name isEqualToString:@"InPlay"] &&
        [self.currentRegion.name isEqualToString:@"Hand"]) {
        if ([dropRegion shouldAddCard:self fromRegion:self.currentRegion]) {
            [self.gameScene cast:self onTarget:[(RWInPlayRegion *)dropRegion cardAtPoint:self.position]];
            return;
        }        
    }
    
    // if card is dragged to a point that is not part of a drop region
    // return to initial position
    if (!dropRegion) {
        [self returnToHomePosition];
        [self cardHint];
        return;
    }    
    
    // card is dropped into the same region
    // maybe should reorder, not in at this point
    if (dropRegion == self.currentRegion) {
        // so that animation doesn't slide the card under another card
        self.zPosition = 19;
        [self returnToHomePosition];
        if ([self.currentRegion.name isEqualToString:@"Hand"]) { [self cardHint]; }
        return;
    }
    
    // dropped in a new region, ask if it can move
    if (dropRegion != self.currentRegion) {
        if ( [dropRegion shouldAddCard:self fromRegion:self.currentRegion] ) {
            [dropRegion addCard:self fromRegion:self.currentRegion];
        } else {
            [self returnToHomePosition];
        }
    }
}

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event {
    
}

@end
