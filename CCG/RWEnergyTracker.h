//
//  RWEnergyTracker.h
//  CCG
//
//  Created by Brian Broom on 2/27/14.
//  Copyright (c) 2014 Brian Broom. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>
#import "RWCardRegion.h"

@class RWPlayer;
@class RWStarTracker;

@interface RWEnergyTracker : SKSpriteNode <RWCardRegion>

@property (weak, nonatomic) RWPlayer *player;
@property (weak, nonatomic) RWStarTracker *starTracker;
@property (assign, nonatomic) BOOL hasPlayedEnergyCardThisTurn;

- (void)addStarFromCard:(RWCard *)card;
- (void)resetCards;

@end
