//
//  RWTextOverlay.h
//  CCG
//
//  Created by Brian Broom on 2/27/14.
//  Copyright (c) 2014 Brian Broom. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

@interface RWTextOverlay : SKSpriteNode

- (void)notifyWithString:(NSString *)message andRotation:(CGFloat)rotation;

@end
