//
//  RWCard.h
//  CCG
//
//  Created by Brian Broom on 2/26/14.
//  Copyright (c) 2014 Brian Broom. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>
#import "RWCardNames.h"
#import "RWCardRegion.h"

@class RWPlayer;

@interface RWCard : SKSpriteNode

@property (assign, nonatomic) RWCardName cardName;
@property (copy, nonatomic) NSString *largeCardFileName;
@property (assign, nonatomic, readonly) CGFloat rotationSign;

@property (copy, nonatomic, readonly) NSString *cardType;
@property (assign, nonatomic, readonly) NSInteger energyCost;
@property (assign, nonatomic, readonly) NSInteger attackValue;
@property (assign, nonatomic, readonly) NSInteger defenseValue;
@property (assign, nonatomic, readonly) NSInteger damage;

@property (weak, nonatomic) RWPlayer *player;

@property (assign, nonatomic) BOOL moved;
@property (assign, nonatomic) CGPoint preMovePosition;
@property (strong, nonatomic) SKNode<RWCardRegion> *currentRegion;
@property (assign, nonatomic, getter = isActive) BOOL active;
@property (assign, nonatomic) BOOL attackedThisTurn;
@property (assign, nonatomic, getter = isIncapacitated) BOOL incapacitated;
@property (strong, nonatomic) NSString *spellTargets;


+ (instancetype)cardWithCardName:(RWCardName)name;
- (instancetype)initWithCardName:(RWCardName)name;

- (void)faceUp;
- (void)faceDown;
- (void)highlight;
- (void)clearHilight;

- (void)discard;
- (void)discardAllAttachemnts;
- (void)attachCard:(RWCard *)attachment;
- (void)haveAttachmentsExpired;
- (void)restackAttachments;
- (void)doDamage:(NSInteger)damage;

@end
