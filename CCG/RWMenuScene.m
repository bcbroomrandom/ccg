//
//  RWMenuScene.m
//  CCG
//
//  Created by Brian Broom on 2/25/14.
//  Copyright (c) 2014 Brian Broom. All rights reserved.
//

@import AVFoundation;

#import "RWMenuScene.h"
#import "RWTutorialScene.h"
#import "RWGameScene.h"
#import "RWCreditScene.h"

@interface RWMenuScene ()

@property (strong, nonatomic) AVAudioPlayer *musicPlayer;

@end

@implementation RWMenuScene

-(id)initWithSize:(CGSize)size {
    if (self = [super initWithSize:size]) {
        /* Setup your scene here */
        
        self.backgroundColor = [SKColor colorWithRed:0.15 green:0.15 blue:0.3 alpha:1.0];
        
        SKSpriteNode *bg = [SKSpriteNode spriteNodeWithImageNamed:@"MenuBackground.png"];
        bg.position = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMidY(self.frame));
        [self addChild:bg];
        
        SKSpriteNode *playButton = [SKSpriteNode spriteNodeWithImageNamed:@"PlayButton.png"];
        playButton.position = CGPointMake(650, 330);
        playButton.name = @"play";
        [self addChild:playButton];
        
        SKSpriteNode *tutorialButton = [SKSpriteNode spriteNodeWithImageNamed:@"TutorialButton.png"];
        tutorialButton.position = CGPointMake(650, 450);
        tutorialButton.name = @"tutorial";
        [self addChild:tutorialButton];
        
        SKSpriteNode *creditsButton = [SKSpriteNode spriteNodeWithImageNamed:@"CreditsButton.png"];
        creditsButton.position = CGPointMake(650, 210);
        creditsButton.name = @"credits";
        [self addChild:creditsButton];
        
        SKSpriteNode *linkButton = [SKSpriteNode spriteNodeWithImageNamed:@"LinkButton.png"];
        linkButton.position = CGPointMake(CGRectGetMidX(self.frame), 70);
        linkButton.name = @"link";
        [self addChild:linkButton];
        
        NSString *path = [[NSBundle mainBundle] pathForResource:@"Take a Chance" ofType:@"mp3"];
        NSURL *musicFile = [[NSURL alloc] initFileURLWithPath:path];
        NSError *error = nil;
        _musicPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:musicFile  error:&error];
        _musicPlayer.numberOfLoops = -1; // negative value repeats indefinitely
        [_musicPlayer prepareToPlay];
        [_musicPlayer play];
    }
    return self;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [touches anyObject];
    CGPoint location = [touch locationInNode:self];
    SKNode *touchNode = [self nodeAtPoint:location];
    
    SKAction *pushDown = [SKAction scaleTo:0.8 duration:0.2];
    SKAction *click = [SKAction playSoundFileNamed:@"click.wav" waitForCompletion:YES];
    SKAction *pushUp = [SKAction scaleTo:1.0 duration:0.1];
    
    SKAction *clickAndUp = [SKAction group:@[click, pushUp]];
    SKAction *push = [SKAction sequence:@[pushDown, clickAndUp]];
    
    if ([touchNode.name isEqualToString:@"link"]) {
        [touchNode runAction:push completion:^{
            [self.musicPlayer stop];
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://www.raywenderlich.com/"]];
        }];
    }
    
    if ([touchNode.name isEqualToString:@"tutorial"]) {
        [touchNode runAction:push completion:^{
            [self.musicPlayer stop];
            SKScene *tutorialScene = [[RWTutorialScene alloc] initWithSize:self.size];
            [self.view presentScene:tutorialScene];
        }];
    }
    
    if ([touchNode.name isEqualToString:@"play"]) {
        [touchNode runAction:push completion:^{
            [self.musicPlayer stop];
            SKScene *gameScene = [[RWGameScene alloc] initWithSize:self.size];
            [self.view presentScene:gameScene];
        }];
    }
    
    if ([touchNode.name isEqualToString:@"credits"]) {
        [touchNode runAction:push completion:^{
            [self.musicPlayer stop];
            SKScene *creditScene = [[RWCreditScene alloc] initWithSize:self.size];
            [self.view presentScene:creditScene];
        }];
    }
}



@end
