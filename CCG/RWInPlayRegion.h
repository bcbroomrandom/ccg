//
//  RWInPlayRegion.h
//  CCG
//
//  Created by Brian Broom on 2/27/14.
//  Copyright (c) 2014 Brian Broom. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>
#import "RWCardRegion.h"

@class RWPlayer;

@interface RWInPlayRegion : SKSpriteNode <RWCardRegion>

@property (weak, nonatomic) RWPlayer *player;

- (NSArray *)creatures;
- (NSArray *)fighters;
- (void)clearHilights;
- (void)resetAttacks;
- (void)checkCreaturesForExpiredAttachments;
- (RWCard *)cardAtPoint:(CGPoint)point;
//- (BOOL)canAttackAtPoint:(CGPoint)point;
- (void)attackCardAtPoint:(CGPoint)point withCard:(RWCard *)attacker;

@end
