//
//  RWPlayerTracker.h
//  CCG
//
//  Created by Brian Broom on 2/27/14.
//  Copyright (c) 2014 Brian Broom. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

@class RWPlayer;

@interface RWPlayerTracker : SKSpriteNode

- (void)doDamage:(NSInteger)damage toPlayer:(RWPlayer *)player;
- (NSInteger)healthForPlayer:(RWPlayer *)player;

@end
