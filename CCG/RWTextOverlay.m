//
//  RWTextOverlay.m
//  CCG
//
//  Created by Brian Broom on 2/27/14.
//  Copyright (c) 2014 Brian Broom. All rights reserved.
//

#import "RWTextOverlay.h"
#import "UIColor+CCGColors.h"
#import "DSMultilineLabelNode.h"

@interface RWTextOverlay ()

//@property (strong, nonatomic) SKLabelNode *overlayLabel;
@property (strong, nonatomic) DSMultilineLabelNode *textLabel;

@end

@implementation RWTextOverlay

- (id)initWithColor:(UIColor *)color size:(CGSize)size {
    self = [super initWithColor:[UIColor regionBackgroundColor] size:size];
    if (self) {
        self.zPosition = -20;
        self.userInteractionEnabled = YES;
        
        _textLabel = [DSMultilineLabelNode labelNodeWithFontNamed:@"OpenSans"];
        _textLabel.fontSize = 50;
        _textLabel.text = @" ";
        _textLabel.fontColor = [UIColor blackColor];
        _textLabel.name = @"text";
        [self addChild:_textLabel];
        
        SKLabelNode *overlayLabel = [SKLabelNode labelNodeWithFontNamed:@"OpenSans"];
        overlayLabel.fontSize = 40;
        overlayLabel.fontColor = [UIColor blackColor];
        overlayLabel.position = CGPointMake(0, -300);
        overlayLabel.text = @"Tap to dismiss.";
        [self addChild:overlayLabel];
    }
    return self;
}

#pragma mark - Overlay

- (void)notifyWithString:(NSString *)message andRotation:(CGFloat)rotation {
    self.zPosition = 20;
    self.zRotation = rotation;
    self.textLabel.text = message;
}

- (void)dismissDisplay {
    self.textLabel.text = @" ";
    self.zPosition = -20;
}

#pragma mark - Touch Handlers

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [self dismissDisplay];
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    
}

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event {
    
}

@end
