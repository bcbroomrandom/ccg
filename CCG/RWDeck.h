//
//  RWDeck.h
//  CCG
//
//  Created by Brian Broom on 2/26/14.
//  Copyright (c) 2014 Brian Broom. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

@class RWPlayer;
@class RWCard;

@interface RWDeck : SKSpriteNode

@property (assign, nonatomic, getter = isActive) BOOL active;

@property (assign, nonatomic) BOOL hasDrawnThisTurn;
@property (weak, nonatomic) RWPlayer *player;

- (id)initWithDataArray:(NSArray *)cardNumberArray;
- (void)shuffle;
- (RWCard *)drawCard;

@end
