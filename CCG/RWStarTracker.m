//
//  RWStarTracker.m
//  CCG
//
//  Created by Brian Broom on 2/27/14.
//  Copyright (c) 2014 Brian Broom. All rights reserved.
//

#import "RWStarTracker.h"
#import "UIColor+CCGColors.h"

@interface RWStarTracker ()

@property (strong, nonatomic) SKTexture *blankStarTexture;
@property (strong, nonatomic) SKTexture *filledStarTexture;
@property (strong, nonatomic) NSArray *starImages;

@end

@implementation RWStarTracker

- (id)init {
    self = [super initWithColor:[UIColor clearColor] size:CGSizeMake(250, 35)];
    if (self) {
        _stars = 0;
        
        _blankStarTexture = [SKTexture textureWithImageNamed:@"icon_star_transparent.png"];
        _filledStarTexture = [SKTexture textureWithImageNamed:@"icon_star_filled.png"];
        
        SKSpriteNode *star1 = [SKSpriteNode spriteNodeWithTexture:_blankStarTexture];
        star1.position = CGPointMake(-90, 0);
        [self addChild:star1];
        
        SKSpriteNode *star2 = [SKSpriteNode spriteNodeWithTexture:_blankStarTexture];
        star2.position = CGPointMake(-50, 0);
        [self addChild:star2];
        
        SKSpriteNode *star3 = [SKSpriteNode spriteNodeWithTexture:_blankStarTexture];
        star3.position = CGPointMake(-5,0);
        [self addChild:star3
         ];
        
        SKSpriteNode *star4 = [SKSpriteNode spriteNodeWithTexture:_blankStarTexture];
        star4.position = CGPointMake(40, 0);
        [self addChild:star4];
        
        SKSpriteNode *star5 = [SKSpriteNode spriteNodeWithTexture:_blankStarTexture];
        star5.position = CGPointMake(80, 0);
        [self addChild:star5];
        
        _starImages = @[star1, star2, star3, star4, star5];
    }
    return self;
}

- (BOOL)canAddStar {
    return (self.stars < 5);
}

- (void)addStar {
    SKSpriteNode *image = (SKSpriteNode *)self.starImages[self.stars];
    image.texture = self.filledStarTexture;
    self.stars += 1;
}

- (void)removeStars:(NSInteger)used {
    if (used > self.stars) { NSLog(@"Error: remove stars called with to many stars"); }
        
    for (NSInteger i = self.stars - used; i < self.stars; i++) {
        SKSpriteNode *image = (SKSpriteNode *)self.starImages[i];
        image.texture = self.blankStarTexture;
    }
    self.stars -= used;
}

- (void)resetStars {
    _stars = 0;
    
    for (NSInteger i=0; i<5; i++) {
        SKSpriteNode *image = (SKSpriteNode *)self.starImages[i];
        image.texture = self.blankStarTexture;
    }
}




@end
