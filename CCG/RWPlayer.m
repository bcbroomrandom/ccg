//
//  RWPlayer.m
//  CCG
//
//  Created by Brian Broom on 2/27/14.
//  Copyright (c) 2014 Brian Broom. All rights reserved.
//

#import "RWPlayer.h"
#import "RWDeck.h"
#import "RWEnergyTracker.h"

@implementation RWPlayer

- (id)init {
    self = [super init];
    if (self) {
        
    }
    return self;
}

//TODO should this be a custom setter?
- (void)makeActive {
    self.deck.active = YES;
    self.active = YES;
    self.energyTracker.hasPlayedEnergyCardThisTurn = NO;
}

- (void)makeInactive {
    self.deck.active = NO;
    self.active = NO;
}

@end
